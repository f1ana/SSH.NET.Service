﻿namespace SSHNETService.Configuration {
    public class SSHPortForwardingItem {
        public string localHostname { get; set; }
        public uint localPort { get; set; }
        public string remoteHostname { get; set; }
        public uint remotePort { get; set; }
    }
}