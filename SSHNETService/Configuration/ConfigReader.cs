﻿using System.Configuration;
using System.IO;
using Newtonsoft.Json;

namespace SSHNETService.Configuration {
    public static class ConfigReader {
        public static SSHConnectionConfig[] ReadConfig() {
            var filePath = ConfigurationManager.AppSettings["jsonConfigFile"];
            if (string.IsNullOrEmpty(filePath) || !File.Exists(filePath))
                throw new ConfigurationErrorsException("Unable to find configuration file.");

            var contents = File.ReadAllText(filePath);
            return JsonConvert.DeserializeObject<SSHConnectionConfig[]>(contents);
        }
    }
}