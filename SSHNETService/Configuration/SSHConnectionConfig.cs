﻿namespace SSHNETService.Configuration {
    public class SSHConnectionConfig {
        public SSHConnectionConfig() {
            portForwardingItems = new SSHPortForwardingItem[0];
        }

        public string endpoint { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string keyPath { get; set; }
        public int keepAliveInterval { get; set; }
        public SSHPortForwardingItem[] portForwardingItems { get; set; }
    }
}