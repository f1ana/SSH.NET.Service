﻿using System.Configuration;
using System.IO;
using log4net.Config;

namespace SSHNETService.Configuration {
    public static class ConfigLogger {
        public static void Configure() {
            string fileName = ConfigurationManager.AppSettings["log4net-path"];
            if (string.IsNullOrEmpty(fileName)) {
                return;
            }

            XmlConfigurator.ConfigureAndWatch(new FileInfo(fileName));
        }
    }
}