﻿using Renci.SshNet;
using SSHNETService.Configuration;

namespace SSHNETService.Factory {
    public static class SSHClientFactory {
        public static SshClient CreateClient(SSHConnectionConfig config) {
            if (!string.IsNullOrEmpty(config.password)) {
                return new SshClient(config.endpoint, config.username, config.password);
            }
            if (!string.IsNullOrEmpty(config.keyPath)) {
                return new SshClient(config.endpoint, config.username, new PrivateKeyFile(config.keyPath));
            }
            return null;
        }
    }
}