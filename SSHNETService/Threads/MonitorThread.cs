﻿using System.Threading;
using System.Threading.Tasks;
using SSHNETService.Abstract;
using SSHNETService.Configuration;

namespace SSHNETService.Threads {
    public class MonitorThread : ThreadBase {
        private SSHConnectionConfig[] sshConfigs;
        private Worker[] workers;
        private Thread[] workerThreads;

        private void setupLogging() {
            SafelyExecute(Delegate_setupLogging);
        }

        private void Delegate_setupLogging() {
            ConfigLogger.Configure();
        }

        private void readConfiguration() {
            SafelyExecute(Delegate_readConfiguration, Delegate_readConfigurationFail);
        }

        private void Delegate_readConfiguration() {
            LogInformation("Reading configuration...");
            sshConfigs = ConfigReader.ReadConfig();
            LogInformation("Configuration read!");
        }

        private void Delegate_readConfigurationFail() {
            sshConfigs = null;
            LogError("Configuration could not be read.");
        }

        private void startAllThreads() {
            LogInformation($"Need to start {sshConfigs.Length} threads!");

            workerThreads = new Thread[sshConfigs.Length];
            workers = new Worker[sshConfigs.Length];

            var i = 0;
            foreach (var config in sshConfigs) {
                var j = i;

                LogInformation($"Starting thread {j}...");
                workers[i] = new Worker();
                workerThreads[i] = new Thread(() => workers[j].doWork(config));
                workerThreads[i].Name = $"WorkerThread{i}";
                workerThreads[i].Start();
                LogInformation($"Thread {j} started!");
                i++;
            }
        }

        private void stopAllThreads() {
            LogInformation("Asking all threads to stop...");
            Parallel.ForEach(workers, (w) => {
                w.RequestStop();
            });
            Parallel.ForEach(workerThreads, (wt) => {
                LogInformation($"Waiting for thread {wt.Name} to join...");
                wt.Join();
            });
        }

        private void checkAllThreads() {
            for (var i = 0; i < sshConfigs.Length; i++) {
                if (workerThreads[i].ThreadState != ThreadState.Stopped) {
                    continue;
                }

                LogInformation($"Thread {i} stopped unexpectedly.  Restarting...");
                var j = i;
                workers[i] = new Worker();
                workerThreads[i] = new Thread(() => workers[j].doWork(sshConfigs[j]));
                workerThreads[i].Name = $"WorkerThread{i}";
                workerThreads[i].Start();
            }
        }

        public override void doWork(SSHConnectionConfig config = null) {
            setupLogging();
            LogInformation("Logging started!");
            readConfiguration();

            if (sshConfigs?.Length < 1) {
                LogError("Parsed configuration does not have any tunnels.");
                return;
            }

            LogInformation("Starting all threads...");
            startAllThreads();

            while (!StopRequested) {
                Thread.Sleep(1000);
                checkAllThreads();
            }

            LogInformation("Request to stop service received.");
            stopAllThreads();
        }
    }
}