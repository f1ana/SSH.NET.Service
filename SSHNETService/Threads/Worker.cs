﻿using System;
using System.Threading;
using Renci.SshNet;
using Renci.SshNet.Common;
using SSHNETService.Abstract;
using SSHNETService.Configuration;
using SSHNETService.Factory;

namespace SSHNETService.Threads {
    public class Worker : ThreadBase {

        private void portExceptionHandler(object sender, ExceptionEventArgs e) {
            LogError(e.Exception);
        }

        private void Delegate_doWork(SSHConnectionConfig config) {
            LogInformation($"Connecting to SSH endpoint: {config.endpoint}");
            using (var client = SSHClientFactory.CreateClient(config)) {
                if (config.keepAliveInterval > 0) {
                    LogInformation($"Setting Keep Alive Interval to {config.keepAliveInterval}s");
                    client.KeepAliveInterval = TimeSpan.FromSeconds(config.keepAliveInterval);
                }
                client.Connect();

                var fpl = new ForwardedPortLocal[config.portForwardingItems.Length];
                var i = 0;

                foreach (var pf in config.portForwardingItems) {
                    LogInformation($"Setting up port forwarding.  Local hostname: {pf.localHostname}  Local port: {pf.localPort}");
                    var port = new ForwardedPortLocal(pf.localHostname, pf.localPort, pf.remoteHostname, pf.remotePort);

                    client.AddForwardedPort(port);

                    port.Exception += portExceptionHandler;
                    port.Start();

                    fpl[i++] = port;
                }

                while (!StopRequested) {
                    Thread.Sleep(1000);
                }

                LogInformation("Disconnecting from SSH endpoint...");

                foreach (var fp in fpl) {
                    fp.Stop();
                    fp.Exception -= portExceptionHandler;
                }
                client.Disconnect();

                LogInformation("Disconnected.");
            }
        }

        private void Delegate_doWorkFailed() {
            StopRequested = true;
        }

        public override void doWork(SSHConnectionConfig config = null) {
            if (config == null) {
                throw new ArgumentException("config");
            }

            SafelyExecute(() => Delegate_doWork(config), Delegate_doWorkFailed);
        }
    }
}