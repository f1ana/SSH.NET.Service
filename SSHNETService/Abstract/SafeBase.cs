﻿using System;
using System.Configuration;
using log4net;

namespace SSHNETService.Abstract {
    public abstract class SafeBase {
        private readonly ILog _logger = LogManager.GetLogger(ConfigurationManager.AppSettings["log4net-logger"]);

        protected void SafelyExecute(Action method, Action failMethod = null) {
            try {
                method();
            }
            catch (Exception e) {
                LogError(e);
                failMethod?.Invoke();
            }
        }

        protected T SafelyExecute<T>(Func<T> method, Func<T> failMethod = null) {
            try {
                return method();
            }
            catch (Exception e) {
                LogError(e);
                return failMethod != null ? failMethod() : default(T);
            }
        }

        protected void LogError(Exception e) {
            _logger.Error(e);
        }

        protected void LogError(string error) {
            _logger.Error(error);
        }

        protected void LogInformation(string info) {
            _logger.Info(info);
        }
    }
}