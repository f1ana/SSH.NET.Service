﻿using SSHNETService.Configuration;

namespace SSHNETService.Abstract {
    public abstract class ThreadBase : SafeBase {
        protected volatile bool StopRequested;

        public void RequestStop() {
            StopRequested = true;
        }

        public abstract void doWork(SSHConnectionConfig config = null);
    }
}