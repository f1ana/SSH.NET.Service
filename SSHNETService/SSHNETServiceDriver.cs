﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SSHNETService.Abstract;
using SSHNETService.Threads;

namespace SSHNETService
{
    public class SSHNETServiceDriver
    {
        public static void Main() {
            ThreadBase hb = new MonitorThread();
            Thread hbt = new Thread(() => hb.doWork());
            hbt.Name = "Monitor";
            hbt.Start();

            Console.ReadLine();

            hb.RequestStop();
            hbt.Join();

        }
    }
}
