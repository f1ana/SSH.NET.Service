﻿using System.ServiceProcess;
using System.Threading;
using SSHNETService.Abstract;
using SSHNETService.Threads;

namespace SSHNETService {
    public partial class SSHNETService : ServiceBase {
        private ThreadBase hb;
        private Thread hbt;

        public SSHNETService() {
            InitializeComponent();
        }

        protected override void OnStart(string[] args) {
            hb = new MonitorThread();
            hbt = new Thread(() => hb.doWork()) {Name = "Monitor"};
            hbt.Start();
        }

        protected override void OnStop() {
            hb.RequestStop();
            hbt.Join();
        }
    }
}