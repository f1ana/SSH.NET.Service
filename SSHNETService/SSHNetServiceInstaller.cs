﻿using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace SSHNETService {
    [RunInstaller(true)]
    public class SSHNetServiceInstaller : Installer {
        public SSHNetServiceInstaller() {
            var process = new ServiceProcessInstaller {Account = ServiceAccount.LocalService};

            var service = new ServiceInstaller {
                StartType = ServiceStartMode.Automatic,
                ServiceName = "SSHNETService",
                DisplayName = "SSHNETService",
                Description = "Provides automatic SSH connections.",
                DelayedAutoStart = true
            };

            Installers.Add(service);
            Installers.Add(process);
        }
    }
}